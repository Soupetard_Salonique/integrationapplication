# Intégration d'Application

## 1 - Utilisation de l'API

Une fois le projet cloné, lancer CaillouApplication.

Routes disponibles :

- localhost:8080/basket?email=[email]&numBasket=[numBasket]
    > Affiche le contenu du panier selectionné avec les produits présents dans ce panier et les qualités et défauts du panier.
- localhost:8080/addToBasket?code=[codeBar]&email=[email]&numBasket=[numBasket]
    > Ajoute un produit au panier selectionné (plusieurs ajouts du même produit sont possibles)
- localhost:8080/removeFromBasket?code=[codeBar]&email=[email]&numBasket=[numBasket]
    > Enlève la première occurence du produit

## 2 - Présentation de l'application
Cette application est réalisée avec Spring Boot et permet d'obtenir des informations de nutritions de différents produits.
Cette application récupérera des données sur les produits auprès d'une API publique, Open Food Facts. Elle mesurera les qualités nutritives des produits.
J'ai utilisé Unirest pour réaliser l'API.

## 3 - Fonctionalités

#### 0 : API REST
L'application est une API REST.

#### 1 : Calculer les qualités et les défauts de chaque produit
Avec comme seule information un code barre, l'application fournie les qualités et les défauts du produit afin de renvoyer cette synthèse.(voir Qualités et défauts).

#### 2 : Calcul du Score nutritionnel
L'application calcule le score nutritionnel

#### 3 : Sauvegarde des informations d'un utilisateur et d'un panier
En plus du code barre, on doit pouvoir fournir une adresse mail et un numéro de panier afin de pouvoir enregistrer les produits mis au panier par un consomateur. Toutes ces informations sont donc sauvegardées.

#### 4 : Synthèse de panier
L'application fournira une aggregation des qualités et des défauts des produits composant le panier.

#### 5 : Suppression d'un produit d'un panier
L'application permettra de supprimer un produit d'un panier d'un utilisateur.

## 4 - Open Food Facts
L'API d'Open Food Facts est disponible à l'adresse suivante :
https://fr.openfoodfacts.org/data
Exemple : https://fr.openfoodfacts.org/api/v0/produit/3029330003533.json
La documentation de l'API est diponible ici : https://en.wiki.openfoodfacts.org/API

## 5 - Score nutritionnel

Le score nutritionnel des aliments repose sur le calcul d’un score unique et global prenant en
compte, pour chaque aliment :
- une composante négative N,
- une composante positive P.

La composante N du score prend en compte les éléments nutritionnels suivants:
de limiter la consommation : densité énergétique (apport calorique en kJ pour 100 g d’aliment),
teneurs en acides gras saturés (AGS), en sucres simples (en g pour 100g d’aliment) et en sel (en
mg pour 100g d’aliment). Sa valeur correspond à la somme des points attribués, de 1 à 10, en
fonction de la teneur de la composition nutritionnelle de l’aliment (cf. tableau 1). La note pour la
composante N peut aller de 0 à 40.

Tableau 1 : Points attribués à chacun des éléments de la composante dite « négative » N

| Points | Densité énergétique (kJ/100g) (energy_100g) | Graisses saturées (g/100g) (saturated-fat_100g) | Sucres simples (g/100g) (sugars_100g) | Sodium1 (mg/100g) (salt_100g) |
| ------ | ----------------------------- | -------------------------- | ----------------------- | ----------------- |
| 0      | < 335                         | < 1                        | < 4,5                   | < 90              |
| 1      | > 335                         | > 1                        | > 4,5                   | > 90              |
| 2      | > 670                         | > 2                        | > 9                     | > 180             |
| 3      | > 1005                        | > 3                        | > 13,5                  | > 270             |
| 4      | > 1340                        | > 4                        | > 18                    | > 360             |
| 5      | > 1675                        | > 5                        | > 22,5                  | > 450             |
| 6      | > 2010                        | > 6                        | > 27                    | > 540             |
| 7      | > 2345                        | > 7                        | > 31                    | > 630             |
| 8      | > 2680                        | > 8                        | > 36                    | > 720             |
| 9      | > 3015                        | > 9                        | > 40                    | > 810             |
| 10     | > 3350                        | > 10                       | > 45                    | > 900             |

- La composante P est calculée, en fonction de la teneur de l’aliment en fibres et en protéines (exprimées en g pour 100 g d’aliment). Pour chacun de ces éléments, des points, allant de 1 à 5 sont attribués en fonction de leur teneur dans l’aliment (cf. tableau 2). La composante positive P du score nutritionnel est la note correspondant à la somme des points définis pour ces deux éléments : cette note est donc comprise entre 0 et 10.

Tableau 2 : Points attribués à chacun des nutriments de la composante dite positive

| Points | Fibres (g/100g) (fiber_100g) | Protéines (g/100g) (proteins_100g) |
| ------ | --------------- | ------------------ |
| 0      | < 0,9           | < 1,6              |
| 1      | > 0,9           | > 1,6              |
| 2      | > 1,9           | > 3,2              |
| 3      | > 2,8           | > 4,8              |
| 4      | > 3,7           | > 6,4              |
| 5      | > 4,7           | > 8,0              |

Calcul du score nutritionnel

**Score nutritionnel = Total Points N – Total Points P**

La note finale du score nutritionnel attribuée à un aliment est donc susceptible d’être comprise entre une valeur de - 10 (le plus favorable sur le plan nutritionnel) et une valeur théorique de + 40 (le plus défavorable sur le plan nutritionnel).

Classement de l’aliment dans l’échelle nutritionnelle à cinq niveaux sur la base du score calculé

| Classe    | Bornes du score |
| --------- | --------------- |
| Trop Bon  | Min à -1        |
| Bon       | 0 à 2           |
| Mangeable | 3 à 10          |
| Mouai     | 11 à 18         |
| Degueu    | 19 à Max        |


## 6 - Qualités et défauts
L'application peut lister pour chaque produit ses qualités et ses défauts basés sur les tableaux 1 et 2 listés précédemment. Les composantes négatives (tableau 1) sont classées suivant ces règles :
 - Considéré comme une qualité si les points du nutriment <=3
 - Considéré comme un défaut si les points du nutriment >= 7

 En ce qui concerne les composantes positives (tableau 2), elles sont classées suivant ces règles :
 - Considéré comme une qualité si les points du nutriment >= 2
 - Considéré comme un défaut si les points du nutriment <= 0
